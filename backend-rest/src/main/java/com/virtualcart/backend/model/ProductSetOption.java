package com.virtualcart.backend.model;

import com.virtualcart.backend.Enum.ClientLevelEnum;
import com.virtualcart.backend.Enum.PromoCodeEnum;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 2/19/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductSetOption {
    private Integer minPoints;
    private ClientLevelEnum clientLevel;
    private PromoCodeEnum promoCode;

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public ClientLevelEnum getClientLevel() {
        return clientLevel;
    }

    public void setClientLevel(ClientLevelEnum clientLevel) {
        this.clientLevel = clientLevel;
    }

    public PromoCodeEnum getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(PromoCodeEnum promoCode) {
        this.promoCode = promoCode;
    }
}
