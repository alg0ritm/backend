package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SimpleUser.class)
public abstract class SimpleUser_ {

    public static volatile SingularAttribute<SimpleUser, Long> id;
    public static volatile SingularAttribute<SimpleUser, String> lastname;
    public static volatile SingularAttribute<SimpleUser, String> firstname;

}

