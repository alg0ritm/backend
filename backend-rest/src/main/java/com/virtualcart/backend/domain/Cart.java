package com.virtualcart.backend.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"cart_id\"")
    private Long id;

    @ManyToOne(targetEntity = Client.class, cascade = CascadeType.ALL)
    private Client client;

    @OneToMany(mappedBy = "cart")
    /*@JoinTable
       (
           name="cart",
           joinColumns={ @JoinColumn(name="cart_cart_id", referencedColumnName="cart_id") }
       )*/
    private List<BoughtProduct> boughtProducts;


    public List<BoughtProduct> getBoughtProducts() {
        return boughtProducts;
    }

    public void setBoughtProducts(List<BoughtProduct> boughtProducts) {
        this.boughtProducts = boughtProducts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


}
