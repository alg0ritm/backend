package com.virtualcart.backend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


@Entity
@NamedQuery(name = "Client.findByName", query = "select u from Client u where u.firstname = :firstname and u.lastname = :lastname")
public class Client {

    @Id
    @Column(name = "\"pers_code\"")
    private Long personalCode;

    @Column(name = "\"firstname\"")
    private String firstname;

    @Column(name = "\"lastname\"")
    private String lastname;

    @Column(name = "\"points\"")
    private long points;

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
    /*@OneToMany
	private Set<Cart> carts = new HashSet<Cart>();

	public Set<Cart> getCarts() {
		return carts;
	}
	public void setCarts(Set<Cart> carts) {
		this.carts = carts;
	}*/


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long id) {
        this.personalCode = id;
    }


}
