package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cart.class)
public abstract class Cart_ {

    public static volatile SingularAttribute<Cart, Long> id;
    public static volatile SingularAttribute<Cart, Client> client;
    public static volatile ListAttribute<Cart, BoughtProduct> boughtProducts;

}

