package com.virtualcart.backend.domain;

import com.virtualcart.backend.Enum.ClientLevelEnum;
import com.virtualcart.backend.iface.MappedEnum;
import com.virtualcart.backend.iface.SystemDictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 2/16/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "client_level")
@MappedEnum(enumClass = ClientLevelEnum.class)
public class ClientLevel extends SystemDictionary {

    @Column
    private long ptsAmount;

    public long getPtsAmount() {
        return ptsAmount;
    }

    public void setPtsAmount(long ptsAmount) {
        this.ptsAmount = ptsAmount;
    }
}
