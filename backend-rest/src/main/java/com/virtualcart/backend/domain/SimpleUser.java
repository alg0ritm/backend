package com.virtualcart.backend.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "app_users2") // @Table is optional, but "user" is a keyword in many SQL variants
// for postgre works
/*@NamedQuery(name="SimpleUser.findByName", query = "select u from SimpleUser u where u.name = :name")
public class SimpleUser implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="user_id",sequenceName="\"app_users2_id_seq\"",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id" )
	@Column(name="\"id\"")
    private Long id = null;*/

// for mysql works
/*@NamedQuery(name="SimpleUser.findByName", query = "select u from SimpleUser u where u.name = :name")
public class SimpleUser implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="\"id\"")
    private Long id = null;*/

@NamedQuery(name = "SimpleUser.findByName", query = "select u from SimpleUser u where u.firstname = :firstname and u.lastname = :lastname")
public class SimpleUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"id\"")
    private Long id = null;

    @Column(name = "\"firstname\"")
    private String firstname;

    @Column(name = "\"lastname\"")
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastName) {
        this.lastname = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
