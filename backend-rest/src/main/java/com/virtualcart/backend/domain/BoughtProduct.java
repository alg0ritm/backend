package com.virtualcart.backend.domain;

import javax.persistence.*;

@Entity
public class BoughtProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"bought_product_id\"")
    private Long id;

    @ManyToOne(targetEntity = Cart.class, cascade = CascadeType.ALL)
    private Cart cart;

    @ManyToOne(targetEntity = Product.class, cascade = CascadeType.ALL)
    private Product product;

    @Column
    private int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

}
