package com.virtualcart.backend.domain;

import com.virtualcart.backend.Enum.PromoCodeEnum;
import com.virtualcart.backend.iface.MappedEnum;
import com.virtualcart.backend.iface.SystemDictionary;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 3/9/13
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "promo_code")
@MappedEnum(enumClass = PromoCodeEnum.class)
public class PromoCode extends SystemDictionary {
}
