package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BoughtProduct.class)
public abstract class BoughtProduct_ {

    public static volatile SingularAttribute<BoughtProduct, Product> product;
    public static volatile SingularAttribute<BoughtProduct, Long> id;
    public static volatile SingularAttribute<BoughtProduct, Integer> quantity;
    public static volatile SingularAttribute<BoughtProduct, Cart> cart;

}

