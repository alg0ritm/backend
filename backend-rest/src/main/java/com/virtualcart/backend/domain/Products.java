package com.virtualcart.backend.domain;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 3/22/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class Products {

    @JsonProperty("products")
    private List<Product> products;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @JsonProperty("product")
    private Product product;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
