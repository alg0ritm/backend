package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductSetConn.class)
public abstract class ProductSetConn_ {

    public static volatile SingularAttribute<ProductSetConn, Product> product;
    public static volatile SingularAttribute<ProductSetConn, Integer> id;
    public static volatile SingularAttribute<ProductSetConn, ProductSet> productSet;

}

