package com.virtualcart.backend.domain;

import com.virtualcart.backend.iface.SystemDictionary;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemDictionary.class)
public abstract class SystemDictionary_ {

    public static volatile SingularAttribute<SystemDictionary, Integer> id;
    public static volatile SingularAttribute<SystemDictionary, String> code;

}

