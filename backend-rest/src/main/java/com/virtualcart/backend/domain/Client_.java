package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

    public static volatile SingularAttribute<Client, Long> personalCode;
    public static volatile SingularAttribute<Client, String> lastname;
    public static volatile SingularAttribute<Client, Long> points;
    public static volatile SingularAttribute<Client, String> firstname;

}

