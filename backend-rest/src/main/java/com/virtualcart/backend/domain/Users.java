package com.virtualcart.backend.domain;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 2/3/13
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Users {

    @JsonProperty("users")
    private List<SimpleUser> users;

    public List<SimpleUser> getUsers() {
        return users;
    }

    public void setUsers(List<SimpleUser> users) {
        this.users = users;
    }
}
