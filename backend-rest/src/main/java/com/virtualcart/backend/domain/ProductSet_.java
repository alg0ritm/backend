package com.virtualcart.backend.domain;

import com.virtualcart.backend.Enum.ClientLevelEnum;
import com.virtualcart.backend.Enum.PromoCodeEnum;
import com.virtualcart.backend.Enum.PromoEnum;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductSet.class)
public abstract class ProductSet_ {

    public static volatile SingularAttribute<ProductSet, Integer> id;
    public static volatile SingularAttribute<ProductSet, Long> minPoints;
    public static volatile SingularAttribute<ProductSet, ClientLevelEnum> minLevel;
    public static volatile SingularAttribute<ProductSet, Integer> active;
    public static volatile SingularAttribute<ProductSet, PromoEnum> promoType;
    public static volatile SingularAttribute<ProductSet, PromoCodeEnum> promoCode;

}