package com.virtualcart.backend.domain;

import com.virtualcart.backend.Enum.PromoEnum;
import com.virtualcart.backend.iface.MappedEnum;
import com.virtualcart.backend.iface.SystemDictionary;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/27/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "promo_type")
@MappedEnum(enumClass = PromoEnum.class)
public class PromoType extends SystemDictionary {
}
