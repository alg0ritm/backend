package com.virtualcart.backend.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ClientLevel.class)
public abstract class ClientLevel_ extends SystemDictionary_ {

    public static volatile SingularAttribute<ClientLevel, Long> ptsAmount;

}

