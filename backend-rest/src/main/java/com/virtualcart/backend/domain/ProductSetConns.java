package com.virtualcart.backend.domain;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 3/22/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductSetConns {

    @JsonProperty("productSetConns")
    private List<ProductSetConn> productSetConns;

    public List<ProductSetConn> getProductSetConns() {
        return productSetConns;
    }

    public void setProductSetConns(List<ProductSetConn> productSetConns) {
        this.productSetConns = productSetConns;
    }

}
