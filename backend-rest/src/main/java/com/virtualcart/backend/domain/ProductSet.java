package com.virtualcart.backend.domain;

import com.virtualcart.backend.Enum.ClientLevelEnum;
import com.virtualcart.backend.Enum.PromoCodeEnum;
import com.virtualcart.backend.Enum.PromoEnum;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/26/13
 * Time: 11:20 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class ProductSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"product_set_id\"", nullable = false, unique = true)
    private Integer id;

    @Column(name = "promo_type_id", nullable = false, unique = false)
    @Enumerated(EnumType.ORDINAL)
    private PromoEnum promoType;

    @Column(name = "min_points")
    private long minPoints;

    @Column(name = "min_level")
    @Enumerated(EnumType.ORDINAL)
    private ClientLevelEnum minLevel;

    @Column
    private int active;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public PromoCodeEnum getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(PromoCodeEnum promoCode) {
        this.promoCode = promoCode;
    }

    @Column(name = "promo_code_id")
    @Enumerated(EnumType.ORDINAL)
    private PromoCodeEnum promoCode;

    public long getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(long minPoints) {
        this.minPoints = minPoints;
    }

    public ClientLevelEnum getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(ClientLevelEnum minLevel) {
        this.minLevel = minLevel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PromoEnum getPromoType() {
        return promoType;
    }

    public void setPromoType(PromoEnum promoType) {
        this.promoType = promoType;
    }
}
