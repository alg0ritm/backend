package com.virtualcart.backend.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/26/13
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSetConn implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"product_set_conn_id\"", nullable = false, unique = true)
    private Integer id;

    @ManyToOne(targetEntity = Product.class, cascade = CascadeType.ALL)
    private Product product;

    @ManyToOne(targetEntity = ProductSet.class, cascade = CascadeType.ALL)
    private ProductSet productSet;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductSet getProductSet() {
        return productSet;
    }

    public void setProductSet(ProductSet productSet) {
        this.productSet = productSet;
    }
}
