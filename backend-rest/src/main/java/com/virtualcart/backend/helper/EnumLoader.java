package com.virtualcart.backend.helper;

import com.virtualcart.backend.iface.MappedEnum;
import com.virtualcart.backend.iface.SessionAction;
import com.virtualcart.backend.iface.SystemDictionary;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/26/13
 * Time: 11:48 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class EnumLoader implements SessionAction {
    private EntityManager entityManager;

    @PersistenceContext
    void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;

    }


    @Override
    public void run() {

        Set<EntityType<?>> mappingList = entityManager.getMetamodel().getEntities();
        for (EntityType<?> mapping : mappingList) {
            Class<?> clazz = mapping.getJavaType();
            if (!SystemDictionary.class.isAssignableFrom(clazz))
                continue;
            if (!clazz.isAnnotationPresent(MappedEnum.class))
                continue;

            MappedEnum mappedEnum = clazz.getAnnotation(MappedEnum.class);
            updateEnumIdentifiers(mappedEnum.enumClass(), (Class<SystemDictionary>) clazz);
        }
    }

    private void updateEnumIdentifiers(
            Class<? extends Enum> enumClass,
            Class<? extends SystemDictionary> entityClass) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery = cb.createQuery();
        Root<? extends SystemDictionary> from = criteriaQuery.from(entityClass);
        CriteriaQuery<Object> select = criteriaQuery.select(from);
        TypedQuery<Object> typedQuery = entityManager.createQuery(select);


        List<Object> valueList =
                (List<Object>) typedQuery.getResultList();

        int maxId = 0;
        Enum[] constants = enumClass.getEnumConstants();
        Iterator valueIterator = valueList.iterator();
        while (valueIterator.hasNext()) {
            SystemDictionary value = (SystemDictionary) valueIterator.next();

            int valueId = value.getId().intValue();
            setEnumOrdinal(Enum.valueOf(enumClass, value.getCode()), valueId);
            if (valueId > maxId)
                maxId = valueId;
        }

        Object valuesArray = Array.newInstance(enumClass, maxId + 1);
        for (Enum value : constants)
            Array.set(valuesArray, value.ordinal(), value);

        Field field;
        try {
            field = enumClass.getDeclaredField("$VALUES");
            field.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.set(null, valuesArray);
        } catch (Exception ex) {
            try {
                throw new Exception("Can't update values array: ", ex);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private void setEnumOrdinal(Enum object, int ordinal) {
        Field field;
        try {
            field = object.getClass().getSuperclass().getDeclaredField("ordinal");
            field.setAccessible(true);
            field.set(object, ordinal);
        } catch (Exception ex) {
            try {
                throw new Exception("Can't update enum ordinal: " + ex);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}
