package com.virtualcart.backend.service;

import com.virtualcart.backend.domain.Cart;
import com.virtualcart.backend.domain.Client;
import com.virtualcart.backend.domain.SimpleUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserService {

    private EntityManager entityManager;

    /**
     * Set the entity manager. Assumes automatic dependency injection via the
     * JPA @PersistenceContext annotation. However this method may still be
     * called manually in a unit-test.
     *
     * @param entityManager
     */
    @PersistenceContext
    void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Client> getUsers() {
        return entityManager.createQuery("select u from SimpleUser u")
                .getResultList();
    }

    //@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void createUser(String firstname) {
        SimpleUser user = new SimpleUser();
        user.setFirstname(firstname);
        user.setLastname("TEST4");
        entityManager.persist(user);
    }

    @Transactional(readOnly = false)
    public void createCart(String firstname) {

        Client client = new Client();
        client.setFirstname(firstname);
        client.setLastname("TEST-CART4");
        double rnd = Math.random();
        long x = (long) rnd;
        client.setPersonalCode(x);
        //entityManager.persist(client);
        Cart cart = new Cart();
        cart.setClient(client);
        entityManager.persist(cart);

    }

}
