package com.virtualcart.backend.service;

import com.virtualcart.backend.Enum.PromoEnum;
import com.virtualcart.backend.domain.*;
import com.virtualcart.backend.helper.ProductServiceHelper;
import com.virtualcart.backend.model.ProductSetOption;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ProductService {

    private EntityManager entityManager;

    @Autowired
    ProductServiceHelper productServiceHelper;

    @Value("${redis.connection.url}")
    private String redisConnectionUrl;

    @PersistenceContext
    void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
        entityManager.getMetamodel().getEntities();
    }

    public List<BoughtProduct> getBoughtProducts() {
        return entityManager.createQuery("select u from BoughtProduct u")
                .getResultList();
    }

    public Object getProducts() {
        return entityManager.createQuery("select u from Product u")
                .getResultList();
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void saveProduct(Product product) {
        entityManager.persist(product);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void saveBoughtProduct(BoughtProduct boughtProduct) {
        entityManager.persist(boughtProduct);
    }

    @Transactional
    public void savePromoCompaignSet(PromoEnum promoType, Set<Product> productSet, Integer minPoints) {


        ProductSet productSetDomain = new ProductSet();
        productSetDomain.setPromoType(promoType);

        for (Product product : productSet) {

            ProductSetConn promoSetConn = new ProductSetConn();

            if (minPoints != null) {
                productSetDomain.setMinPoints(minPoints);
            }
            //entityManager.persist(product);
            //entityManager.persist(productSetDomain);
            promoSetConn.setProduct(product);
            promoSetConn.setProductSet(productSetDomain);
            entityManager.persist(promoSetConn);
        }
        //Pattern.compile("")

    }

    public List<ProductSet> getProductSet(ProductSetOption productSetOption) {

        Predicate predicate = null;

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProductSetConn> productSetConnExp = query.from(ProductSetConn.class);
        Join<ProductSetConn, ProductSet> productSet = productSetConnExp.join("productSet");

        Predicate criteria = cb.conjunction();
        if (productSetOption.getMinPoints() > 0) {
            criteria = cb.and(criteria, cb.equal(productSet.get(ProductSet_.minPoints), productSetOption.getMinPoints()));
        }
        if (productSetOption.getClientLevel() != null) {
            criteria = cb.and(criteria, cb.equal(productSet.get(ProductSet_.minLevel), productSetOption.getClientLevel()));
        }
        if (productSetOption.getPromoCode() != null) {
            criteria = cb.and(criteria, cb.equal(productSet.get(ProductSet_.promoCode), productSetOption.getPromoCode()));
        }

        criteria = cb.and(criteria, cb.equal(productSet.get(ProductSet_.active), 1));


        query.multiselect(productSetConnExp.get("productSet")).distinct(true);
        query.where(criteria);

        Query q = entityManager.createQuery(query);
        List result = q.getResultList();

        return result;

    }

    public ProductSet getProductSet(int productSetNum) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProductSet> productSetConnExp = query.from(ProductSet.class);
        //Join<ProductSetConn, ProductSet> productSet = productSetConnExp.join("productSet");
        query.select(productSetConnExp);
        query.where(cb.equal(productSetConnExp.get(ProductSet_.id), productSetNum));


        Query q = entityManager.createQuery(query);

        return (ProductSet) q.getSingleResult();
    }

    public List getProductSetConn(int productSetNum) {
        ProductSet productSet = getProductSet(productSetNum);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProductSetConn> productSetConnExp = query.from(ProductSetConn.class);
        //Join<ProductSetConn, Product> product = productSetConnExp.join("product");
        query.select(productSetConnExp);
        query.where(cb.equal(productSetConnExp.get(ProductSetConn_.productSet), productSet));
        Query q = entityManager.createQuery(query);
        return (List<ProductSetConn>) q.getResultList();
       /* String query = "SELECT * FROM testdb.productsetconn psc "+
        "inner join testdb.productset ps on psc.productSet_product_set_id = ps.product_set_id "+
        "inner join product p on psc.product_product_id = p.product_id "+
        "where psc.productSet_product_set_id = 96;";*/

        /*Query queryGetCompany = this.entityManager.createNativeQuery(query);
        List companyList = queryGetCompany.getResultList();

        return companyList;*/
    }

    public Object getProductSetConnRedis(int id, int clientId) {

        /*List<ProductSetConn> productSetConn = productService.getProductSetConn(productSetNum);

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);

        if (productSetConn.size() > 0) {
            jedis.set("productSets_user1_date1", mapper.writeValueAsString(productSetConn));
            String values = jedis.get("productSets_user1_date1");
            mapper.writeValue(new File("user_productSets_user1_date1.json"), values);

            //through mapper itself
            ArrayList productSetConn1 = (ArrayList) mapper.readValue(mapper.writeValueAsString(productSetConn), List.class);
            assertNotNull(productSetConn1);

            //From redis
            ArrayList productSetConn2 = (ArrayList) mapper.readValue(jedis.get("productSets_user1_date1"), List.class);
            assertNotNull(productSetConn2);
        }*/
        return null;
    }

    public Product getProduct(Long id) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Product> product = query.from(Product.class);

        query.select(product);
        query.where(cb.equal(product.get(Product_.id), id));

        Query q = entityManager.createQuery(query);
        Product result = (Product) q.getSingleResult();

        return result;


    }

    public ProductSet getProductSetConnFromDatabase(int productSetNum) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProductSetConn> productSetConnExp = query.from(ProductSetConn.class);
        Join<ProductSetConn, ProductSet> productSet = productSetConnExp.join("productSet");
        query.select(productSetConnExp.get("productSet"));
        query.where(cb.equal(productSet.get(ProductSet_.id), productSetNum));


        Query q = entityManager.createQuery(query);

        return (ProductSet) q.getSingleResult();
    }


    public Object getProductSetConnFromRedis(int id) {

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);
        ArrayList productSetConn2 = new ArrayList();

        try {
            productSetConn2 = (ArrayList) mapper.readValue(jedis.get("productSets_100_products"), List.class);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return productSetConn2;
    }
}
