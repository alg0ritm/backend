package com.virtualcart.backend.iface;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/26/13
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MappedEnum {
    Class<? extends Enum> enumClass();
}
