package com.virtualcart.backend.iface;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/26/13
 * Time: 11:50 PM
 * To change this template use File | Settings | File Templates.
 */
@MappedSuperclass
public class SystemDictionary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"id\"", nullable = false, unique = true)
    private Integer id;

    @Column(name = "code", nullable = false, unique = true, length = 32)
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
