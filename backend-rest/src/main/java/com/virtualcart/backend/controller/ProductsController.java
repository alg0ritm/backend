package com.virtualcart.backend.controller;

import com.virtualcart.backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 3/22/13
 * Time: 5:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ProductsController {

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
     public String users(Model model) {
        model.addAttribute("products", productService.getProducts());
        return "products";
    }
    @RequestMapping(value = "/products/{productId}", method = RequestMethod.GET)
    public String getProduct(Model model, @PathVariable("productId") Long id) {
        model.addAttribute("product", productService.getProduct(id));
        return "product";
    }

    @RequestMapping(value = "/products/campaign/redis/{seqn}", method = RequestMethod.GET)
    public String getCompaignProductsFromRedis(Model model, @PathVariable("seqn") int id) {
        //return productService.getProductSetConn(id);
        model.addAttribute("productSetConns", productService.getProductSetConnFromRedis(id));
        return "productSetConns";
    }

    @RequestMapping(value = "/products/campaign/database/{seqn}", method = RequestMethod.GET)
    public String getCompaignProductsFromDatabase(Model model, @PathVariable("seqn") int id) {
        //return productService.getProductSetConn(id);
        model.addAttribute("productSetConns", productService.getProductSetConn(id));
        return "productSetConns";
    }

    @RequestMapping(value = "/products/campaign/{seqn}/{clientId}", method = RequestMethod.GET)
    public String getCompaignProductsFromRedis(Model model, @PathVariable("seqn") int id, @PathVariable("clientId") int clientId) {
        //return productService.getProductSetConn(id);
        model.addAttribute("productSetConns", productService.getProductSetConnRedis(id, clientId));
        return "productSetConns";
    }

}
