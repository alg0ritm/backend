package com.virtualcart.backend.controller;


import com.virtualcart.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping("/users")
    public String users(Model model) {
        model.addAttribute("users", userService.getUsers());
        return "users";
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.GET)
    public String createUser(Model model) {
        return "create-user";
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    @Transactional
    public String createUser(Model model, String firstname) {
        userService.createUser(firstname);
        return "redirect:/virtualcart/users";
    }

    @RequestMapping(value = "/create-cart", method = RequestMethod.GET)
    public String createCart(Model model) {
        return "create-cart";
    }

    @RequestMapping(value = "/create-cart", method = RequestMethod.POST)
    @Transactional
    public String createCart(Model model, String firstname) {
        userService.createCart(firstname);
        return "redirect:/virtualcart/users";
    }
}