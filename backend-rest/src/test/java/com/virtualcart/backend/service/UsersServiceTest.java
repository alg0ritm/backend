package com.virtualcart.backend.service;

import com.virtualcart.backend.domain.SimpleUser;
import com.virtualcart.backend.domain.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/application-context.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class UsersServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private UserService userService;

	@Test
	public void getUsersTest() {
		assertNotNull(userService.getUsers());
	}

	@Test
	public void createCart() {
		userService.createCart("ANTON");
	}

	@Test
	@Rollback(true)
	public void createUser() throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		userService.createUser("ANTON2");
	}


    @Test
    public void getUsersJSON() {

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());

        Users users = restTemplate.getForObject("http://192.168.11.108:8085/virtualcart/users.json", Users.class);
        List<SimpleUser> usersList = users.getUsers();

        assertNotNull(usersList);

        for(SimpleUser simpleUser : usersList) {
            System.out.println("USER NAME " + simpleUser.getFirstname());
        }
    }

}
