package com.virtualcart.backend.service;

import com.virtualcart.backend.Enum.PromoCodeEnum;
import com.virtualcart.backend.Enum.PromoEnum;
import com.virtualcart.backend.domain.*;
import com.virtualcart.backend.model.ProductSetOption;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/application-context.xml"})
public class ProductServiceTest {

    public static final int MAX_PRODUCTS_COUNT = 100;
    @Autowired
    private ProductService productService;

    @Value("${redis.connection.url}")
    private String redisConnectionUrl;

    @Test
    public void getBoughtProducts() {
        assertNotNull(productService.getBoughtProducts());
    }

    @Test
    public void getProducts() {
        assertNotNull(productService.getProducts());
    }

    @Test
    public void saveProduct() throws Exception {
        Product product = new Product();
        product.setPrice(new BigDecimal("30.00"));
        product.setTitle("TEST PRODUCT");
        product.setDescription("Description for test product");

        BoughtProduct boughtProduct = new BoughtProduct();
        boughtProduct.setProduct(product);

        try {
            productService.saveProduct(product);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test
    public void saveBoughtProduct() throws Exception {
        Product product = new Product();
        product.setPrice(new BigDecimal("30.00"));
        product.setTitle("TEST PRODUCT");
        product.setDescription("Description for test product");

        BoughtProduct boughtProduct = new BoughtProduct();
        boughtProduct.setProduct(product);

        try {
            productService.saveBoughtProduct(boughtProduct);
        } catch (Exception e) {
            throw e;
        }


    }


    @Test
    public void mergeClient() {

    }

    @Test
    @Rollback(true)
    public void saveNewPromoCompaignSet() {
        Product product = null;
        Set<Product> productSet = new HashSet<Product>();

        for(int i=0; i< MAX_PRODUCTS_COUNT; i++) {
            product = new Product();
            product.setTitle("TEST PRODUCT " + (int)(Math.random()* MAX_PRODUCTS_COUNT));
            product.setDescription("TEST PRODUCT FOR SET NUMBER " + i);
            product.setPrice(new BigDecimal("33.00"));
            productSet.add(product);
        }
       /* product.setTitle("TEST PRODUCT FOR SET1");
        product.setDescription("TEST PRODUCT FOR SET DESC2");
        product.setPrice(new BigDecimal("33.00"));
        Product product1 = new Product();
        product1.setTitle("TEST PRODUCT FOR SET2");
        product1.setDescription("TEST PRODUCT FOR SET DESC4");
        product1.setPrice(new BigDecimal("34.00"));
        Set<Product> productSet = new HashSet<Product>();
        productSet.add(product);
        productSet.add(product1);*/
        productService.savePromoCompaignSet(PromoEnum.PROMO_CODE, productSet, null);
    }

    @Test
    @Rollback(true)
    public void savePromoCompaignSetByPointsPurely() {
        Integer minPoints = MAX_PRODUCTS_COUNT;
        Product product = new Product();
        product.setTitle("TEST PRODUCT FOR SET BY POINTS PURELY 11");
        product.setDescription("TEST PRODUCT FOR SET BY POINTS PURELY 11");
        product.setPrice(new BigDecimal("112.00"));
        Product product1 = new Product();
        product1.setTitle("TEST PRODUCT FOR SET BY POINTS PURELY 12");
        product1.setDescription("TEST PRODUCT FOR SET BY POINTS PURELY 12");
        product1.setPrice(new BigDecimal("113.00"));
        Set<Product> productSet = new HashSet<Product>();
        productSet.add(product);
        productSet.add(product1);
        //productService.savePromoCompaignSet(PromoEnum.PLANNED_SALE, productSet, minPoints);
    }

    @Test
    //@Ignore
    public void getProductSetBasedOnCriteria() {
        ProductSetOption productSetOption = new ProductSetOption();
        productSetOption.setMinPoints(MAX_PRODUCTS_COUNT);
        //productSetOption.setClientLevel(ClientLevelEnum.BRONZE);
        productSetOption.setPromoCode(PromoCodeEnum.MEDIUM_SALE_tok3);
        List<ProductSet> productSet = productService.getProductSet(productSetOption);
        assertNotNull(productSet);
    }

    @Test
    public void getStProductSet() {
        int productSetNum = 1;
        ProductSet productSet = productService.getProductSet(productSetNum);
        assertNotNull(productSet);

    }

    @Test
    public void getAllProductsFromProductSet() throws IOException {
        int productSetNum = 3;
        List<ProductSetConn> productSetConn = productService.getProductSetConn(productSetNum);

        assertNotNull(productSetConn);

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);

        if (productSetConn.size() > 0) {
            jedis.set("productSets_user2_date1", mapper.writeValueAsString(productSetConn));
            String values = jedis.get("productSets_user2_date1");

            //From redis
            ArrayList productSetConn2 = (ArrayList) mapper.readValue(jedis.get("productSets_user2_date1"), List.class);
            assertNotNull(productSetConn2);
        }
    }

    @Test
    public void selectProductSetFromDb() throws IOException {
        int productSetNum = 95;
        List<ProductSetConn> productSetConn = productService.getProductSetConn(productSetNum);
        assertNotNull(productSetConn);
    }

    @Test
    public void storeProductsToRedis() throws IOException {
        int productSetNum = 97;
        List<ProductSetConn> productSetConn = productService.getProductSetConn(productSetNum);

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);

        if (productSetConn.size() > 0) {
            jedis.set("productSets_100_products", mapper.writeValueAsString(productSetConn));
            //String values = jedis.get("productSets_1000_products");
            //mapper.writeValue(new File("productSets_1000_products.json"), values);

            //through mapper itself
            /*ArrayList productSetConn1 = (ArrayList) mapper.readValue(mapper.writeValueAsString(productSetConn), List.class);
            assertNotNull(productSetConn1);*/

            //From redis
            /*ArrayList productSetConn2 = (ArrayList) mapper.readValue(jedis.get("productSets_1000_products"), List.class);
            assertNotNull(productSetConn2);*/
        }
    }

    @Test
    public void getProductSetFromRedis() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);
        //String redisValue = jedis.get("productSets_1000_products");

        ArrayList productSetConn2 = (ArrayList) mapper.readValue(jedis.get("productSets_10000_products"), List.class);
        assertNotNull(productSetConn2);
    }

    @Test
    public void setRedisComplexObejctJson() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        Jedis jedis = new Jedis(redisConnectionUrl);
        Date now = new Date();

        jedis.sadd("users", "user:anton");

        Map<String, String> map = new HashMap<String, String>();
        map.put("firstname", "anton");
        map.put("lastname", "vesselov");
        map.put("username", "anton");
        jedis.hmset("user:anton", map);

        jedis.sadd("users", "user:random");

        Map<String, String> map1 = new HashMap<String, String>();
        map1.put("firstname", "random");
        map1.put("lastname", "random123");
        map1.put("username", "random1234");
        jedis.hmset("user:random", map1);

        Set values = jedis.smembers("users");

        for (Object value : values) {
            mapper.writeValue(new File("user.json"), jedis.hgetAll((String) value));
        }
    }

    @Test
    public void setRedis3StoryedObjectJson() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Jedis jedis = new Jedis(redisConnectionUrl);
        Date now = new Date();
        jedis.sadd("user_1", "compaign1");
        Set values = jedis.smembers("user_1");
        Set values1 = jedis.smembers(values.iterator().next().toString());
        for (Object value : values1) {
            mapper.writeValue(new File("user3storyed.json"), jedis.hgetAll((String) value));
        }
    }

    @Test
      public void getProductsJSON() {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
        Products products = restTemplate.getForObject("http://192.168.11.5:9966/virtualcart/products.json", Products.class);
        List<Product> poductList = products.getProducts();
        assertNotNull(poductList);

        for(Product product : poductList) {
            System.out.println("Product Title " + product.getTitle());
        }
    }

    @Test
    public void getProductsFromProductSetJSON() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        ProductSetConns productsJson = restTemplate.getForObject("http://192.168.11.5:9966/virtualcart/products/campaign/3.json", ProductSetConns.class);


        assertNotNull(productsJson);

    }

    @Test
    public void getProduct() {
        Product product = (Product) productService.getProduct(new Long("155"));
        assertNotNull(product);
    }

    /*@Test
    public void saveProduct() {

        Product product = (Product) productService.saveProduct(new Long("155"));
        assertNotNull(product);
    }*/






}
