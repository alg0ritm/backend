package com.virtualcart.backend.service;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import redis.clients.jedis.Jedis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 1/24/13
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/application-context.xml"})
public class JedisServiceTest {

    @Value("${redis.connection.url}")
    private String redisConnectionUrl;

    private SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-yyyy");

    @Test
    public void testRedisConnection() {

        Jedis jedis = new Jedis(redisConnectionUrl);
        jedis.set("foo", "bar");
        String value = jedis.get("foo");
        assertNotNull(value);
    }

    @Test
    public void setRedisComplexObejct() {

        Jedis jedis = new Jedis(redisConnectionUrl);
        Date now = new Date();

        System.out.print(sdf.format(now));
        System.out.print("prodcuts:" + sdf.format(now) + ":1111");


        jedis.sadd("prodcuts:" + sdf.format(now) + ":1111", "1236");
        jedis.sadd("prodcuts:" + sdf.format(now) + ":1111", "1237");
        Set values = jedis.smembers("prodcuts:" + sdf.format(now) + ":1111");

        //System.out.println(value);
        //assertEquals(value, "jedis");
    }

    @Test
    public void setRedisComplexObejctJson1() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        Jedis jedis = new Jedis(redisConnectionUrl);
        Date now = new Date();

        jedis.sadd("users123", "user:random12");

        Map<String, String> map1 = new HashMap<String, String>();
        map1.put("firstname", "random1");
        map1.put("lastname", "random1235");
        map1.put("username", "random12345");
        jedis.hmset("user:random12", map1);

        jedis.sadd("users123", "user:anton12");

        Map<String, String> map = new HashMap<String, String>();
        map.put("firstname", "anton1");
        map.put("lastname", "vesselov1");
        map.put("username", "anton1");
        jedis.hmset("user:anton12", map);

        Set values = jedis.smembers("users123") ;

        File file = new File("user4.json");

        ArrayList<String> resultJson = new ArrayList<String>();

        for (Object value : values) {
            Map test = jedis.hgetAll((String) value);
            System.out.println(mapper.writeValueAsString(test));
            resultJson.add(mapper.writeValueAsString(test));
        }

        FileWriter fstream = new FileWriter("user1.json", true); //true tells to append data.
        BufferedWriter out = new BufferedWriter(fstream);

        for(String resultTemp: resultJson) {
            out.write(resultTemp);
            out.write("\n");
        }

        try
        {
            out.close();
        }
        catch (Exception e)
        {
            System.err.println("Error: " + e.getMessage());
        }


    }


}
