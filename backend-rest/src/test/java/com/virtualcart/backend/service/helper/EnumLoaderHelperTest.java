package com.virtualcart.backend.service.helper;

import com.virtualcart.backend.helper.EnumLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/application-context.xml"})
public class EnumLoaderHelperTest {

    @Autowired
    EnumLoader enumLoader;

    @Test
    public void enumLoaderPerfTest() {
        enumLoader.run();
    }

    @Test
    public void  testLongBarcode() {
       assertNotNull(new Long("123456789012"));
    }


}
