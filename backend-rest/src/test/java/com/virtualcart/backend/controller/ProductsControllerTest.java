package com.virtualcart.backend.controller;

import com.virtualcart.backend.domain.ProductSetConns;
import com.virtualcart.backend.domain.Products;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: antonve
 * Date: 4/27/13
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/application-context.xml"})
public class ProductsControllerTest {

    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
    }

    @Test
    public void getProduct() {
        Products products = restTemplate.getForObject("http://192.168.0.32:8085/virtualcart/products/1111221132134681.json", Products.class);
        assertNotNull(products);

    }

    @Test
    public void getProductSetsFromRedis() {
        ProductSetConns productsJson = restTemplate.getForObject("http://192.168.0.32:8085/virtualcart/products/campaign/redis/"+95+".json", ProductSetConns.class);
        assertNotNull(productsJson);

    }

    @Test
    public void getProductSetsFromDatabase() {
        ProductSetConns productsJson = restTemplate.getForObject("http://192.168.0.32:8085/virtualcart/products/campaign/database/"+97+".json", ProductSetConns.class);
        assertNotNull(productsJson);

    }




}
